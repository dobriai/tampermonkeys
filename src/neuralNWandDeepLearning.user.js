// ==UserScript==
// @name         NeuralNWandDeepLearning
// @namespace    http://tampermonkey.net/
// @version      1.0.3
// @description  Widen the main text
// @author       Ivan Dobrianov
// @match        http://neuralnetworksanddeeplearning.com/chap*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=neuralnetworksanddeeplearning.com
// @grant        none
// @homepage     https://gitlab.com/dobriai/tampermonkeys
// ==/UserScript==

(function() {
    'use strict';

    // Create and add a top-level div elem - a new flex container
    const hdrElem = document.getElementsByClassName("header")[0];
    const newDiv = document.createElement("div");
    newDiv.style.display = "flex";
    hdrElem.insertAdjacentElement("afterend", newDiv);

    // Move the "section" into the new flex div
    const sectElem = document.getElementsByClassName("section")[0];
    sectElem.style.flex = "1";
    newDiv.appendChild(sectElem);

    // Move the "toc" next to the "section", so they are side-by-side in the flex div
    const tocElem = document.getElementById("toc");
    tocElem.style.flex = "0 0 0";
    tocElem.style.position = "static";
    tocElem.style.padding = "20px";
    newDiv.appendChild(tocElem);

    // Make the notes appear inside the text - ugly, but easy
    const noteElems = document.getElementsByClassName("marginnote");
    [...noteElems].forEach(ee => {
        ee.style.position = "static";
        ee.style.width = "auto";
        ee.style["font-size"] = "11pt";
    });
})();
