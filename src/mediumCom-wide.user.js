// ==UserScript==
// @name         mediumCom-wide
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Make medium.com articles use full width
// @author       You
// @match        https://medium.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=mozilla.org
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // Add a new sStyle Sheet and add "negating" rules to it
    const newStyle = document.createElement('style');
    document.head.appendChild(newStyle);
    const newRule = '.cq { max-width: none; }'
    newStyle.sheet.insertRule(newRule);

    // There is a pesky floating sidebar now - just remove it
    const postSidebars = [...document.querySelectorAll('div[data-test-id="post-sidebar"]')];
    postSidebars[0].style.display = 'none';
})();