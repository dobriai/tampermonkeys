// ==UserScript==
// @name        Reg_Tests-Filter
// @description Adda menu command to filter down the Reg Test Results based on a file
// @namespace   https://gitlab.com/dobriai/tampermonkeys
// @homepageURL https://gitlab.com/dobriai/tampermonkeys
// @version     1.2
// @author      dobriai
// @match       file://viola/*.html
// @grant       GM_registerMenuCommand
// ==/UserScript==
// --------------------------------------------------------------------------------------
// NOTE: You need to check "Allow access to file URLs" for the VM extension in order to
// be able to @match  file:// URLs!
// This is done via right-click on any extension -> Manage Extensions -> Details for VM
// --------------------------------------------------------------------------------------
(function () {
    const cmdName = 'Violentmonkey: Reg Tests Filter';
    console.log(`Registering command: ${cmdName}`);

    const visit = (node, context) => {
        if (context.depth && node.nodeName === 'TD') {
            const inTxt = node.innerText.toLowerCase().trim();
            const hideMe = inTxt.search(/\w[\w\s\-\.~\\]+\.(txt|testconfig|bat|pl)/i) == 0
                && !(inTxt in context.nameHash);

            // console.log(`${hideMe ? 'Hiding' : 'Not hiding'} |${inTxt}|`);
            return hideMe;
        }

        context = { ...context }; // Insulate parent context
        let hideIt = false;

        const isTr = node.nodeName === 'TR';
        if (isTr)
            context.depth += 1;

        for (let ii = 0; ii < node.children.length && !hideIt; ++ii)
            hideIt = visit(node.children[ii], context);

        if (isTr) {
            if (hideIt)
                context.toHide.push(node); // Don't hide yet - avoid _very_ slow rerenderings!
            return false;
        }
        return hideIt;
    };

    GM_registerMenuCommand("Reg Tetsts Filter", function () {
        console.log(`Executing command: ${cmdName} ...`);

        const input = document.createElement('input');
        input.type = 'file';
        input.onchange = e => {
            const file = e.target.files[0];
            const reader = new FileReader();
            reader.readAsText(file, 'UTF-8');
            reader.onerror = (event) => {
                alert(event.target.error.name);
            };
            reader.onload = readerEvent => {
                const content = readerEvent.target.result; // slurp the whole file
                const allLines = content.split(/\r\n|\n/);                
                const nameHash = {};
                allLines.forEach(line => {
                    if (line.search(/^\s*(\/\/|#)/) != -1 || line.search(/^\s*$/) != -1)
                        return;
                    nameHash[line.toLowerCase().trim()] = 1;
                    // console.log(`Adding to hash|${line.toLowerCase().trim()}|`);
                });
                console.log(`Added ${Object.keys(nameHash).length} tests to be preserved.`)

                const toHide = [];                
                visit(document.body, { depth: 0, nameHash, toHide });
                toHide.forEach(node => node.style.display = 'none');
                
                // console.log(`Finished command: ${cmdName}`);
                alert(`Finished!
                    
                    Tests-to-keep list had ${Object.keys(nameHash).length} lines.
                    We hid ${toHide.length} table rows.`
                );
            }
        }
        input.click();
    }, 'w');
})();
