// ==UserScript==
// @name         adsk-github-fixer
// @namespace    https://gitlab.com/dobriai/tampermonkeys
// @homepageURL  https://gitlab.com/dobriai/tampermonkeys
// @version      0.1
// @description  Fix various stupid shortcomings
// @author       dobriai
// @match        https://git.autodesk.com/*
// @run-at       document-end
// @icon         https://www.google.com/s2/favicons?sz=64&domain=autodesk.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // Let the PR page contents use the full available width
    const elems = document.getElementsByClassName("container-xl");
    for (const elem of elems) {
        elem.style.maxWidth = '8000px';
    }
})();
