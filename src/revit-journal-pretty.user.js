// ==UserScript==
// @name        Revit Journal Formatter
// @description Pretty formatting for Revit journals - a menu command
// @namespace   https://gitlab.com/dobriai/tampermonkeys
// @homepageURL https://gitlab.com/dobriai/tampermonkeys
// @version     1.2.0
// @author      dobriai
// @match       file://*/*.txt
// @match       *://*/*.txt
// @grant       GM_registerMenuCommand
// ==/UserScript==
// --------------------------------------------------------------------------------------
// NOTE: You need to check "Allow access to file URLs" for the VM extension in order to
// be able to @match  file:// URLs!
// This is done via right-click on any extension -> Manage Extensions -> Details for VM
// --------------------------------------------------------------------------------------
(function () {
    const cmdName = 'Violentmonkey: Revit jrn pretty';
    console.log(`Registering command: ${cmdName}`);

    const addCheckBox = (menu, name, cssClass) => {
        const comments = menu.appendChild(document.createElement("div"));
        const commentsOn = comments.appendChild(document.createElement("input"));
        commentsOn.setAttribute("type", "checkbox");
        commentsOn.checked = true;
        commentsOn.onchange = (event) => {
            const val = event.target.checked ? '' : 'none';
            document.querySelectorAll(cssClass).forEach((elem) => { elem.style.display = val; });
        };
        comments.appendChild(document.createTextNode(name));
    };

    // See https://stackoverflow.com/questions/12317049/how-to-split-a-long-regular-expression-into-multiple-lines-in-javascript
    // Note: All Spaces are eaten, thus use '\s' where a space is really needed!
    const createRegExp = (str, opts) => new RegExp(str.raw[0].replace(/\s/gm, ""), opts || "");

    GM_registerMenuCommand("Revit journal pretty", function () {
        console.log(`Executing command: ${cmdName} ...`);

        // Add the needed styles
        const myStyle = document.createElement("style");
        document.head.appendChild(myStyle);
        myStyle.sheet.insertRule("body { font-family: monospace; }", 0);
        myStyle.sheet.insertRule(".rvt-comment { color: Green; }");
        myStyle.sheet.insertRule(".rvt-noise {}");
        myStyle.sheet.insertRule(".rvt-code { white-space: pre-wrap; }");
        myStyle.sheet.insertRule(".rvt-line-num { color: DarkKhaki; }");
        myStyle.sheet.insertRule(".rvt-menu { position: fixed; top: 10px; right: 10px; background-color: navy }");

        // Split the single <pre> child into lines, remove original <pre>
        const root = document.body;
        const lines = root.children[0].innerText.split(/\r?\n/);
        // root.children[0].style.display = 'none';
        root.removeChild(document.body.children[0]);

        // Handle lines one-by-one and format, as needed
        const reComment = /^ *'/;
        const reNoise = createRegExp`API_SUCCESS
            |GUI\sResource\sUsage|Delta\sVM:\sAvail\s
            |\[Jrn\.(?:RegisterExternalServer|AddInManifest|AddInDynamicUpdate|SessionOptions)
            |\w\s\d\d-\w{3}-\d{4}\s(?:\d\d:){2}\d\d\.\d+;\s*\w:<\s*$
            `;
        let iiLine = 0;
        lines.forEach(line => {
            iiLine += 1;
            const myDiv = root.appendChild(document.createElement("div"));
            if (line.search(reComment) != -1) {
                myDiv.classList.add("rvt-comment");
                if (line.search(reNoise) != -1) {
                    myDiv.classList.add("rvt-noise");
                }
            } else {
                myDiv.classList.add("rvt-code");
            }
            const lineNum = myDiv.appendChild(document.createElement("span"));
            lineNum.appendChild(document.createTextNode(iiLine + ' '));
            lineNum.classList.add("rvt-line-num");
            const lineTxt = myDiv.appendChild(document.createElement("span"));
            lineTxt.appendChild(document.createTextNode(line));
        });

        // Add the fixed menu with controls
        const menu = root.appendChild(document.createElement("div"));
        menu.classList.add("rvt-menu");

        addCheckBox(menu, "Line #s", ".rvt-line-num");
        addCheckBox(menu, "Comments", ".rvt-comment");
        addCheckBox(menu, "Noise", ".rvt-noise");
    }, 'w');
})();
