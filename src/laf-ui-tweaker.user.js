// ==UserScript==
// @name         LAF-UI-tweaker
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  Make room for the Load and Cancel buttons in the browser
// @author       dobriai
// @homepageURL  https://gitlab.com/dobriai/tampermonkeys
// @run-at       document-end
// @include        https://laf-revit-*.autodesk.com/eed4ee222f/index.html
// @include        https://laf-revit.autodesk.com/eed4ee222f/index.html
// @include        http://127.0.0.1:3456/*
// @include        http://localhost:3456/*
// @include        http://localhost:3000/*
// @icon         https://www.google.com/s2/favicons?domain=autodesk.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // Make the Load and Cancel buttons show at the bottom of the browser:
    console.log('Ivan is: Tweaking height style of .display-component-service');
    const bigDiv = document.getElementsByClassName('display-component-service')[0];
    bigDiv.style.height = '90vh'
})();
