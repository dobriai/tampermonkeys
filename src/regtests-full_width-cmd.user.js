// ==UserScript==
// @name        Reg_Tests-Full_Width
// @description Add a menu command to expand Reg Test Results to the browser's view full width
// @namespace   https://gitlab.com/dobriai/tampermonkeys
// @homepageURL https://gitlab.com/dobriai/tampermonkeys
// @version     1.1
// @author      dobriai
// @match       file://viola/*.html
// @grant       GM_registerMenuCommand
// ==/UserScript==
// --------------------------------------------------------------------------------------
// NOTE: You need to check "Allow access to file URLs" for the VM extension in order to
// be able to @match  file:// URLs!
// This is done via right-click on any extension -> Manage Extensions -> Details for VM
// --------------------------------------------------------------------------------------
(function() {
    const cmdName = 'Violentmonkey: Reg Tests Results to full width';
    console.log(`Registering command: ${cmdName}`);
  
    GM_registerMenuCommand("Full Width", function() {
        console.log(`Executing command: ${cmdName}`);
        const elems = document.getElementsByClassName("container pull-left");
        const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0)
        elems[0].style.width = vw.toString() + 'px'
    }, 'w');
  })();
  